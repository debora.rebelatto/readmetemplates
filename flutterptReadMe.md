# Nome Projeto Flutter

Aqui temos uma descrição sobre um projeto.
Este é um arquivo README que auxilia desenvolvedores a se prepararem no ambiente que estarão utilizando.
Ele é feito utilizando markdown e um pouco de HTML.

``` bash
git clone <url>
```

# Configurações do ambiente

[Documentação Flutter Install](https://flutter.dev/docs/get-started/install)
[Tutorial Instalação](https://hurricane-grey-9d5.notion.site/Configura-es-do-ambiente-f9a21b85808d4377aa7a0387738d8303)

<p align="center"> Made with ❤️ by Me. </p>