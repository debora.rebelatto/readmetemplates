

- **Windows**

    ## Git para Windows

    A primeira ferramenta a ser instalada é o Git. Para isso você pode clicar no link abaixo:

    [](https://github.com/git-for-windows/git/releases/download/v2.31.1.windows.1/Git-2.31.1-64-bit.exe)

    Ao clicar, o download será iniciado automaticamente.

    Com o instalador baixado, basta executar e ir clicando em **next** até finalizar a instalação.

    A menos que você saiba exatamente o que está fazendo, é recomendado que todas as opções sejam mantidas no padrão.

    ## SDK do Flutter

    Agora você precisará baixar o SDK do Flutter. O download pode ser feito a partir do seguinte link:

    [](https://storage.googleapis.com/flutter_infra/releases/stable/windows/flutter_windows_2.0.4-stable.zip)

    Ao clicar, o download será iniciado automaticamente.

    Após baixado, extraia todo o conteúdo e mova a pasta gerada `flutter` para um caminho de sua preferência menos para pastas que precisam de privilégios elevados como por exemplo `C:\Arquivos de Programas` (é importante que você lembre desse caminho, ele será usado posteriormente). 

    Nesse guia, a pasta estará na raiz do Disco local C como você pode ver na imagem abaixo:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled.png)

    ### Variáveis Ambiente

    Para que seja possível usar a CLI do Flutter no terminal padrão (Windows PoweShell) é necessário configurarmos, na variável PATH, o caminho da pasta `bin` dentro da pasta extraída no passo anterior. Para isso:

    1. No menu iniciar do Windows, pesquise por "Editar as variáveis de ambiente do sistema" e entre no resultado;
    2. Na janela aberta, clique no botão "Variáveis de Ambiente" no canto inferior direito;
    3. Agora você terá uma janela com duas seções: "Variáveis de usuário para `SEU-USUARIO`" E "Variáveis do sistema". Iremos trabalhar com a seção "Variáveis de usuário para `SEU-USUARIO`";
    4. Na seção indicada, selecione a linha da variável **Path** e clique em "Editar":

        ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/path.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/path.png)

    5. Na nova janela, clique em "Novo" e cole o caminho para a pasta `flutter/bin`. 
    Como eu coloquei a minha pasta `flutter` direto na raiz no Disco C, deixarei assim:

        ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%201.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%201.png)

    6. Agora é só ir clicando em "OK" até fechar a janela aberta no passo 1.

    ## Instalando Chocolatey e o JDK

    Para instalar as libs no Windows, vamos utilizar um gerenciador de pacotes do Windows chamado Chocolatey. Esse gerenciador nos possibilita instalar dependências e ferramentas no sistema com poucos comandos e tudo pelo terminal. Execute o powershell como administrador utilizando a tecla Windows + X ou clicando com o botão direito sobre o botão “Iniciar”:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%202.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%202.png)

    Execute o comando abaixo para verificar se você possui permissões para instalar dependências com o terminal: 

    ```powershell
    Get-ExecutionPolicy
    ```

    Caso o retorno desse comando seja diferente de “Restricted”, pule para o próximo passo. Porém, se o retorno for “Restricted”, execute o seguinte comando em seu terminal:

    ```powershell
    Set-ExecutionPolicy AllSigned
    ```

    Agora, execute o seguinte comando para instalar o Chocolatey:

    ```powershell
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    ```

    Agora, teste se a instalação ocorreu corretamente executando o seguinte comando no seu terminal (nada irá acontecer, mas não deve retornar erros):

    Nesse passo pode ser necessário reiniciar seu terminal

    ```powershell
    choco
    ```

    Agora vamos instalar o JDK 11 (LTS):

    Caso esteja com o JDK instalado em sua máquina, certifique-se que sua versão seja a 8 ou mais recente.

    ```powershell
    choco install -y openjdk11
    ```

    Agora reinicie o Powershell e execute o seguinte comando para verificar se instalou corretamente:

    ```powershell
    java -version
    ```

    Se foi apresentado o valor da versão, a instalação foi um sucesso.

    ## Preparativos para o Android Studio

    Crie uma pasta em um local desejado para instalação da SDK (Ex.: `C:\Android\Sdk`). É muito importante que esse caminho não possua espaços ou caracteres especiais pois irá causar erros.

    **Anote esse caminho para ser utilizado posteriormente.**

    Agora, no Painel de Controle do Windows, abra o item “Sistema e Segurança” ou “Sistema”, clique em “Configurações avançadas do sistema”, selecione “Variáveis de ambiente” e clique no botão “Nova variável de ambiente”, indique o nome da variável como **ANDROID_HOME**, adicione o caminho utilizado acima (Ex.: `C:\Android\Sdk`) como segundo parâmetro e clique em OK:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%203.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%203.png)

    Faça o mesmo processo para criar a variável **JAVA_HOME**. Mas primeiramente temos que obter o caminho de instalação do openjdk11.

    A instalação openjdk11 com o Chocolatey pode acabar criando essa variável automaticamente para você. Caso isso tenha ocorrido, confira se o caminho está certo e, caso sim, pode pular para a configuração da variável PATH.

    O caminho padrão é:

    ```powershell
    C:\Program Files\OpenJDK\openjdk-11.[SUA_VERSÃO]
    ```

    Onde o **[SUA_VERSÃO]** representa o valor específico da sua instalação (ex.: `C:\Program Files\OpenJDK\openjdk-11.0.10_9`). Caso tenha dúvidas de qual seja a sua versão, acesse a pasta `C:\Program Files\OpenJDK` e verifique. Com o caminho anotado, basta criar a **JAVA_HOME**.

    Na mesma janela de "Variáveis de ambiente" no Windows, clique na variável **PATH** e então em "Editar". Haverá uma lista de caminhos e você deve adicionar esses quatro novos caminhos no fim da lista:

    ```bash
    %ANDROID_HOME%\emulator
    %ANDROID_HOME%\tools
    %ANDROID_HOME%\tools\bin
    %ANDROID_HOME%\platform-tools
    ```

    Se sua versão do Windows for menor que a 10 sempre coloque ponto-e-virgula ";" no final dos caminhos para eles não se juntarem.

    ## Instalando o Android Studio

    Você pode realizar o download do Android Studio na seguinte URL: 

    [](https://developer.android.com/studio)

    Clique no botão "Download Android Studio", aceite os termos e confirme o download.

    Com o instalador baixado, execute o instalador.

    A primeira janela que deve aparecer é para escolher o que vai ser instalado. Por padrão, a opção Android Studio já vem selecionada. Selecione também a opção **Android Virtual Device** e clique em **Next**.

    Na sequência, será perguntado sobre o local de instalação do Android Studio. Pode deixar o caminho padrão e clicar em **Next**.

    Em seguida, será perguntado sobre a pasta no menu Iniciar. Deixe o padrão e clique em **Install**. Nessa etapa será realizada instalação. Quando terminar, clique em **Next**.

    Por fim, será apresentada a janela de fim da instalação. Deixe a opção Start Android Studio marcada e clique em **Finish**.

    ### Configurando Android Studio

    Com o Android Studio instalado, chegou a hora de fazer a configuração inicial do programa.

    A primeira janela a ser apresentada deve ser perguntando sobre a importação de configurações de outro Android Studio. Selecione a opção **Do not import settings** e clique em **OK**.

    Em seguida, o Android Studio começará a carregar. Em algum ponto do carregamento, será apresentada uma janela sobre compartilhamento de dados anônimos com a Google. Essa opção é pessoal, escolha o que preferir.

    Após o carregamento terminar, deve aparecer uma página de Welcome. Clique em **Next**.

    Na sequência, será pedido o tipo de instalação. Escolha a opção Custom e clique em **Next**.

    Nesse momento, será pedido para escolher a localização do pacote JDK instalado. Clique em ⬇ e verifique se a opção **JAVA_HOME** está apontando para a JDK 11. Se sim, escolha e Clique em **Next**. Caso contrário, clique no no botão ... e escolha a JDK 11 (você pode inclusive utilizar o caminho anotado no [passo anterior]() para te ajudar):

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%204.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%204.png)

    Em seguida, será perguntado sobre qual tema será utilizado. Escolha o que preferir e clique em **Next**.

    Chegamos na etapa mais importante do processo, a instalação do SDK. A janela apresentará algumas opções, marque de acordo com a imagem:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%205.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%205.png)

    - A **SDK** é o pacote que vai possibilitar que seja feito o build da sua aplicação. Por padrão, ele instala a última SDK estável;
    - O **Intel HAXM** é para melhorar a performance da emulação (Se você for utilizar o Hyper-V ou possuir um processador AMD, **não marque essa opção**.
    - O **Android Virtual Device** vai criar um emulador padrão pronto para execução.

    Um fator essencial nessa etapa é o caminho de instalação da SDK. Utilize a pasta que você criou na seção [Preparativos para o Android Studio]() (Ex.: `C:\Android\Sdk`). Não utilize **espaços ou caracteres especiais** pois causará erros mais para frente e o próprio Android Studio alerta se o seu caminho não está bom.

    Se tudo estiver correto, clique em **Next**.

    Na sequência, temos uma janela perguntando sobre a quantidade de RAM que será disponibilizada para que o HAXM utilize. Essa etapa não irá aparecer para todos pois nem todo computador é compatível com esse recurso. Deixe o recomendado pelo programa e clique em **Next**.

    Em seguida, será apresentada uma janela com um resumo de todas as opções escolhidas até aqui. Verifique se está tudo certo, principalmente os caminhos da SDK e do JDK. Clique em **Finish**.

    Por fim, será realizada a instalação das configurações selecionadas. Quando o programa terminar, clique em **Finish**.

    ## Instalando Google USB Driver

    O próximo passo é instalar o **Google USB Driver** para que tudo funcione corretamente. Para isso, com o Android Studio aberto vá em **Configure** e em **SDK Manager**:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%206.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%206.png)

    Na janela que abriu, vá na guia **SDK Tools** e marque a caixinha do **Google USB Driver:** 

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd.png)

    Agora é só clicar em **OK** e depois em **Finish**.

    ## Preparando seu dispositivo físico Android

    Rodar aplicações mobile em um emulador durante o desenvolvimento é uma tarefa que irá exigir um pouco mais da sua máquina que o desenvolvimento de sites, por exemplo. Felizmente, se você possui um dispositivo com Android 4.1 (API level 16) ou superior, é possível usá-lo para testar suas aplicações nele (prefira usar sempre o dispositivo físico dependendo da memória do seu computador).

    Para que seja possível usar o seu dispositivo físico, é necessário realizar uma configuração bem simples nele. Basta ir em configurações e procurar pelas **opções de desenvolvedor**. Se você não encontrou, é provável que seja necessário desbloquear essa opção. Na maioria dos dispositivos, essa opção pode ser desbloqueada clicando três vezes na versão do sistema do seu aparelho (que pode ser encontrada dentro da opção **Sobre**) lembrando que esse caminho pode mudar de acordo com o seu dispositivo.

    Com as **opções de desenvolvedor** desbloqueadas, basta ir até ela, procurar pela opção **Depuração USB** e deixá-la ativa.

    Para garantir que deu tudo certo, conecte seu dispositivo Android no computador, confirme as permissões que forem solicitadas (essas são para a depuração USB que você ativou) e rode no terminal o comando `flutter devices`.

    Se o seu dispositivo foi reconhecido, então deu tudo certo:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%201.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%201.png)

    ## Preparando um emulador Android

    Se você prefere usar um emulador para testar suas aplicações, é necessário que sua máquina tenha pelo menos 8GB de memória, caso contrário, é provável que você enfrente algumas dificuldades como travamentos ao longo da jornada. 

    Bem, agora que você optou pelo emulador, vamos lá.

    Com o Android Studio aberto vá em **Configure** e ****depois em **AVD Manager**:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%207.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%207.png)

    Na nova janela, clique em **Create Virtual Device** e escolha o dispositivo que deseja emular (eu vou escolher o Pixel 2, você pode fazer o mesmo):

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%208.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%208.png)

    Ao clicar em **Next**, na próxima tela você poderá escolher a versão da API para o Android. Recomenda-se escolher a penúltima versão, caso ainda não esteja baixada, clique em **Download** e com o download finalizado, clique em **Finish**. Agora deixe a versão baixada selecionada e clique em **Next**.

    Na próxima tela, escolha o nome para o seu emulador (ou simplesmente deixe o padrão mesmo) e clique em **Finish**.

    De volta na tela do [AVD Manager](), clique no botão para iniciar o emulador que você instalou:  

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%202.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%202.png)

    Com o emulador aberto, rode o comando `flutter devices` no terminal e verifique se o emulador foi reconhecido corretamente. Se sim, então deu tudo certo: 

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%203.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%203.png)

    ## Finalizando

    Agora, iremos aceitar alguns termos do SDK do Flutter, para isso, rode no terminal o comando `flutter doctor --android-licenses`. Basta ir aceitando com `Y` até terminar e tudo estará pronto.

    O próximo passo é [configurar o Visual Studio Code]().

- **Linux**

    ## Ferramentas necessárias

    Para que a instalação seja feita corretamente, é importante ter as seguintes ferramentas instaladas:

    - bash
    - curl
    - file
    - git
    - unzip
    - xz-utils
    - zip

    Para garantir a disponibilidade de todas, você pode rodar o seguite comando:

    ```bash
    sudo apt-get install bash curl file git unzip xz-utils zip
    ```

    Além dessas, iremos precisar também do `libGLU.so.1` que é disponibilizada através do pacote `libglu1-mesa` caso você esteja no Ubuntu/Debian e da `mesa-libGLU` caso você esteja no Fedora. Para garantir que a ferramenta esteja instalada, basta rodar o comando para instalar. Como eu estou no Ubuntu, vou instalar o `libglu1-mesa`:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%209.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%209.png)

    No meu caso, o pacote já estava instalado por padrão e já estava na última versão.

    ## Instalando o Flutter

    Para instalar o Flutter no Linux, iremos usar o `snapd` que é um gerenciador de pacotes. Nas mais comuns distribuições Linux, ele já vem instalado por padrão, então é muito provável que você já possua na sua máquina. Em algumas distribuições como Debian e Fedora será necessário instalar o `snapd`. Você pode checar em quais distribuições será necessário instalar manualmente a partir do link abaixo:

    [Installing snapd | Snapcraft documentation](https://snapcraft.io/docs/installing-snapd)

    Caso a distribuição instalada na sua máquina esteja na seção **Distributions without snap pre-installed** basta clicar nela e você terá um passo a passo bem simples de como instalar.

    ---

    Com o `snapd` instalado, tudo que você precisa fazer para instalar o Flutter na sua máquina é rodar o seguinte comando no terminal:

    ```bash
    sudo snap install flutter --classic
    ```

    ## Flutter Doctor

    Após ter instalado o Flutter rode o comando `flutter doctor` para visualizar um relatório que mostrará qualquer dependência que ainda precisa ser instalada.

    Na primeira vez que o comando for executado, o SDK do Flutter será baixado e instalado de forma automática.

    Se você está seguindo o guia do zero, provavelmente verá o seguinte relatório:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2010.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2010.png)

    Ver tanta coisa em vermelho causa até um pouco de incômodo, não é mesmo? Mas fique tranquilo(a) que nós iremos passar por cada um desses pontos.

    ## Instalando JDK 11 (Java Development Kit)

    ### Ubuntu/Debian

    Agora precisamos instalar a JDK (Java Development Kit) na versão 11 (LTS mais recente) com os seguintes comandos (execute um por vez):

    Se você já tiver o JDK instalado em sua máquina, certifique-se que sua versão seja a 8 ou mais recente.

    ```bash
    sudo add-apt-repository ppa:openjdk-r/ppa
    sudo apt-get update
    sudo apt-get install openjdk-11-jdk
    ```

    Podemos testar a instalação do JDK com o seguinte comando:

    ```bash
    java -version
    ```

    Caso possua outras versões do java, pode selecionar a versão 11 como padrão usando o comando: 

    ```bash
    sudo update-alternatives --config java
    ```

    ### Arch Linux

    Agora precisamos instalar a JDK (Java Development Kit) na versão 11 (LTS mais recente) com o seguinte comando:

    Se você já tiver o JDK instalado em sua máquina, certifique-se que sua versão seja a 8 ou mais recente.

    ```bash
    sudo pacman -S jdk11-openjdk
    ```

    E por fim setar a versão 11 como padrão com o seguinte comando:

    ```bash
    archlinux-java set java-11-openjdk
    ```

    ## Preparativos para o Android Studio

    Crie uma pasta em um local desejado para instalação da SDK. Ex: `~/Android/Sdk`

    **Anote esse caminho para ser utilizado posteriormente**.

    Anote também o endereço de instalação do JDK 11. Exemplo:

    ```bash
    /usr/lib/jvm/java-11-openjdk-amd64
    ```

    Caso não tenha certeza do caminho, busque na pasta `/usr/lib/jvm/` pela pasta referente ao JDK 11, que provavelmente iniciará com **java-11**.

    Com esses caminhos, precisamos configurar algumas variáveis ambiente em nosso sistema. Procure pelo primeiro dos seguintes arquivos existentes no seu sistema: `~/.zshrc` ou `~/.bashrc`, e adicione essas seis linhas no arquivo (de preferência no início):

    Se nenhum desses arquivos existir, crie o `~/.bashrc` caso utilize o Shell padrão ou `~/.zshrc` caso utilize o ZSH.

    ```bash
    export JAVA_HOME=CAMINHO_ANOTADO_COM_SUA_VERSÃO
    export ANDROID_HOME=~/Android/Sdk
    export PATH=$PATH:$ANDROID_HOME/emulator
    export PATH=$PATH:$ANDROID_HOME/tools
    export PATH=$PATH:$ANDROID_HOME/tools/bin
    export PATH=$PATH:$ANDROID_HOME/platform-tools
    ```

    Não esqueça de substituir o valor na linha **JAVA_HOME** pelo caminho [que você anotou anteriormente](). Além disso, caso esteja utilizando uma pasta diferente para a SDK do Android, altere acima.

    ## Instalando o Android Studio

    Agora precisamos fazer o download através da página oficial do Android Studio:  

    [Download Android Studio and SDK tools | Android Developers](https://developer.android.com/studio)

    Vá até a pasta de download e abra o arquivo tar.gz.

    Esse arquivo deve possuir uma pasta **android-studio** dentro. Extraia essa pasta em um local de preferência (Ex.: `~/`).

    Após a extração, adicione a seguinte variável ambiente no seu sistema (no arquivo `~/.zshrc` ou `~/.bashrc`):

    ```bash
    export PATH=$PATH:~/android-studio/bin
    ```

    A adição desse caminho possibilita a execução do Android Studio diretamente pelo terminal com o comando `studio.sh`. Caso tenha extraído em uma pasta diferente ou alterado o nome da pasta, ajuste o path acima para o que você utilizou.

    Agora, abra o terminal (reinicie se já estiver aberto) e execute o seguinte comando:

    ```bash
    studio.sh
    ```

    ## Configurando Android Studio

    A primeira janela a ser apresentada deve ser perguntando sobre a importação de configurações de outro Android Studio. Selecione a opção **Do not import settings** e clique em **OK**.

    Após o carregamento terminar, deve aparecer uma página de Welcome. Clique em **Next**.

    Na sequência, será pedido o tipo de instalação. Escolha a opção **Custom** e clique em **Next**.

    Nesse momento, será pedido para escolher a localização do pacote JDK instalado. Clique em ⬇ e verifique se a opção **JAVA_HOME** está apontando para a JDK 11. Se sim, escolha e Clique em **Next**. Caso contrário, clique no no botão ... e escolha a JDK 11 (você pode inclusive utilizar o [caminho anotado no passo anterior]() para te ajudar):

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2011.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2011.png)

    Em seguida, será perguntado sobre qual tema será utilizado. Escolha o que preferir e clique em **Next**.

    Chegamos na etapa mais importante do processo, a instalação da SDK. A janela apresentará algumas opções, marque de acordo com a imagem:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2012.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2012.png)

    - **SDK** é o pacote que vai possibilitar que seja feito o build da sua aplicação. Por padrão, ele instala a última SDK estável;
    - O **Android Virtual Device** vai criar um emulador padrão pronto para execução.

    Um fator essencial nessa etapa é o caminho de instalação da SDK. Utilize a pasta que você criou na seção [Preparativos para o Android Studio]() (Ex.: `~/Android/Sdk`). Não utilize **espaços ou caracteres especiais** pois causará erros mais para frente.

    Se tudo estiver correto, clique em **Next**.

    Na sequência, temos uma janela avisando sobre a possibilidade de executar o Emulador com melhor performance usando o KVM (Kernel-mode Virtual Machine). Essa etapa não irá aparecer para todos pois nem todo computador é compatível com esse recurso. Caso tenha interesse em instalar essa ferramenta, será ensinado como ao final dessa página. Finalizada essa etapa, clique em **Next**.

    Em seguida, será apresentada uma janela com um resumo de todas as opções escolhidas até aqui. Verifique se está tudo certo, principalmente os caminhos da SDK e do JDK. Clique em Finish.

    Por fim, será realizada a instalação das configurações selecionadas. Quando o programa terminar, clique em **Finish**.

    Se você quiser criar um atalho para executar o Android Studio a partir do menu do Linux, basta clicar em **Configure**>**Create Desktop Entry**.

    ## KVM (Extra)

    Caso o seu Android Studio tenha acusado a possibilidade de instalar o KVM e você pretende executar sua aplicação no Emulador, pode prosseguir com esse tutorial. Caso contrário, pode pular para a seção de [configuração com um dispositivo físico]().

    Se você prefere usar um emulador para testar suas aplicações, é necessário que sua máquina tenha pelo menos 8GB de memória, caso contrário, é provável que você enfrente algumas dificuldades como travamentos ao longo da jornada.

    Bem, agora que você optou pelo emulador, vamos lá.

    Para instalar o KVM, o processo é bem simples. Em sistemas Ubuntu/Debian e Linux Minti, instale o KVM executando o comando:

    ```bash
    sudo apt install qemu-kvm
    ```

    Em seguida, adicione o seu usuário no grupo do KVM:

    ```bash
    sudo adduser $USER kvm
    ```

    Por fim, reinicie (ou deslogue e log novamente) o sistema e execute o comando:

    ```bash
    grep kvm /etc/group
    ```

    Se o resultado for algo semelhante a:

    ```bash
    kvm:x:NUMERO_QUALQUER:SEU_USUARIO
    ```

    O kvm está instalado corretamente e pronto para uso.

    ## Preparando um emulador Android

    Com o Android Studio aberto vá em **Configure** e ****depois em **AVD Manager**:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%207.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%207.png)

    Na nova janela, clique em **Create Virtual Device** e escolha o dispositivo que deseja emular (eu vou escolher o Pixel 2, você pode fazer o mesmo):

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%208.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%208.png)

    Ao clicar em **Next**, na próxima tela você poderá escolher a versão da API para o Android. Recomenda-se escolher a penúltima versão (que no meu caso é a **Q** API 29), caso ainda não esteja baixada, clique em **Download** e com o download finalizado, clique em **Finish**. Agora deixe a versão baixada selecionada e clique em **Next**:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2013.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2013.png)

    Na próxima tela, escolha o nome para o seu emulador (ou simplesmente deixe o padrão mesmo) e clique em **Finish**.

    De volta na tela do [AVD Manager](), clique no botão para iniciar o emulador que você instalou:  

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%202.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%202.png)

    Com o emulador aberto, rode o comando `flutter devices` no terminal e verifique se o emulador foi reconhecido corretamente. Se sim, então deu tudo certo (independente da plataforma, o resultado deve ser o mesmo):

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%203.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/ggusbd%203.png)

    ## Preparando seu dispositivo físico Android

    Rodar aplicações mobile em um emulador durante o desenvolvimento é uma tarefa que irá exigir um pouco mais da sua máquina que o desenvolvimento de sites, por exemplo. Felizmente, se você possui um dispositivo com Android 4.1 (API level 16) ou superior, é possível usá-lo para testar suas aplicações nele (prefira usar sempre o dispositivo físico dependendo da memória do seu computador).

    Para que seja possível usar o seu dispositivo físico, é necessário realizar uma configuração bem simples nele. Basta ir em configurações e procurar pelas **opções de desenvolvedor**. Se você não encontrou, é provável que seja necessário desbloquear essa opção. Na maioria dos dispositivos, essa opção pode ser desbloqueada clicando três vezes seguidas na versão do sistema do seu aparelho (que pode ser encontrada dentro da opção **Sobre**) lembrando que esse caminho pode mudar de acordo com o seu dispositivo.

    Com as **opções de desenvolvedor** desbloqueadas, basta ir até ela, procurar pela opção **Depuração USB** e deixá-la ativa.

    Para garantir que deu tudo certo, conecte seu dispositivo Android no computador, confirme as permissões que forem solicitadas caso apareça (essas são para a depuração USB que você ativou) e rode no terminal o comando `flutter devices`.

    Se o seu dispositivo foi reconhecido, então deu tudo certo:

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2014.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2014.png)

    ## Finalizando

    Agora, iremos aceitar alguns termos do SDK do Flutter, para isso, rode no terminal o comando `flutter doctor --android-licenses`. Basta ir aceitando com `Y` até terminar e tudo estará pronto.

    Caso você enfrente algum erro como `Exception in thread "main" java.lang.NoClassDefFoundError...` ao rodar o comando acima, abra o Android Studio, clique em **Configure**>**SDK Manager** e na guia **SDK Tools** selecione a opção **Android SDK Command-line Tools (lastest)**: 

    ![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2015.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2015.png)

    Feito isso, clique em **Apply** e confirme em **OK** para fazer o download do recurso selecionado. Depois clique em **Finish** e na outra janela clique em **OK**.

    Feito isso, repita o procedimento de rodar o comando `flutter doctor --android-licenses` e aceite todos os acordos com `Y` como mencionado acima.

    O próximo passo é [configurar o Visual Studio Code]().

- **macOS**

    ## Requisitos:

    **Sistema Operacional**: macOS (64-bit)

    **Ferramentas**: Flutter usa o **Git** para download e atualização. É recomendado que você instale o [Xcode](https://developer.apple.com/xcode/), o que inclui o Git, mas você também pode [instalar o Git separadamente](https://git-scm.com/download/mac).

    **Importante**: Se você está instalando o Flutter em um Mac com o [processador M1](https://www.apple.com/mac/m1/), você pode olhar [essas notas complementares](https://github.com/flutter/flutter/wiki/Developing-with-Flutter-on-Apple-Silicon) que o próprio time do Flutter disponibiliza para que você possa integrar o Flutter à nova arquitetura Apple Silicon.

    ## Obtenha o SDK do Flutter

    1. Baixe o seguinte pacote de instalação para obter a versão estável mais recente do Flutter SDK:
    **[Link para download](https://storage.googleapis.com/flutter_infra/releases/stable/macos/flutter_macos_2.0.4-stable.zip)**
    2. Extraia o arquivo em um diretório de sua preferência, por exemplo:

        ```bash
        cd ~/development
        unzip ~/Downloads/flutter_macos_2.0.4-stable.zip

        # Caso você queira seguir esse exemplo e não possui o diretório usado aqui
        # basta rodar o seguinte comando para criar o diretório:
        mkdir ~/development
        ```

    3. Adicione o `flutter` ao path:
        1. Para adicionar o Flutter ao PATH, determine o caminho onde você extraiu o SDK no passo anterior. Seguindo o exemplo acima, o caminho é `~/development/flutter` mas atente-se ao local que você extraiu, em caso de outro diretório.
        2. Abra (ou crie caso não exista) o arquivo `rc` para o seu shell. Rodando `echo $SHELL` no seu terminal é possível ver qual shell você está usando. Se você está usando o Bash, edite o arquivo `~/.bashrc`. Se você está usando Z shell, edite o arquivo `~/.zshrc`. Já se você está usando outro shell, o caminho e o nome do arquivo vão ser diferentes na sua máquina.
        3. Adicione a seguinte linha no aquivo (no início ou no fim do arquivo) e mude `[CAMINHO_DO_DIRETÓRIO_DO_FLUTTER]` para o caminho do Flutter na sua máquina conforme [mencionado aqui]():

            ```bash
            export PATH=$PATH:[CAMINHO_DO_DIRETÓRIO_DO_FLUTTER]/bin
            ```

            Após isso, salve o arquivo, feche o terminal e abra novamente.

        4. Verifique se o comando `flutter` está disponível rodando o seguinte comando:

            ```bash
            which flutter
            ```

    ## Configuração da plataforma

    O macOS suporta o desenvolvimento de aplicativos Flutter tanto no iOS quanto no Android. Complete pelo menos uma das etapas de configurações a seguir ([iOS]() ou [Android]()) para conseguir construir e executar seu primeiro aplicativo Flutter. 

    ### Configuração iOS

    - **Instale o XCode**
        1. Para desenvolver aplicativos Flutter para iOS, você precisa de um Mac com o [Xcode instalado](https://developer.apple.com/xcode/). Se você já possui o Xcode instalado, passe para o próximo passo. 
        2. Configure a CLI do Xcode para usar a versão do Xcode mais recente instalada rodando o seguinte:   

            ```bash
            sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
            sudo xcodebuild -runFirstLaunch
            ```

            Esse é o caminho correto na maioria dos casos, quando você quer usar a versão mais recente do Xcode.

        3. Certifique-se de que o contrato de licença do Xcode esteja aceito. Você pode aceitar a licença ao abrir o Xcode pela primeira vez ou rodando o seguinte comando no terminal:  

            ```bash
            sudo xcodebuild -license
            ```

        **Atenção**: ‎Versões mais antigas que a versão estável mais recente ainda podem funcionar, mas não são recomendadas para o desenvolvimento do Flutter pois dependendo da versão que você possui, pode ser que enfrente erros.

        Com o Xcode instalado, você está pronto para rodar aplicativos Flutter em um dispositivo iOS ou no simulador.

    - **Configurando o simulador iOS**
        1. No seu Mac, encontre o simulador via Spotlight ou rodando o seguinte comando:

            ```bash
            open -a Simulator
            ```

        2. Certifique-se de que o simulador esteja usando um dispositivo 64-bit (iPhone 5s ou superior) checando as configurações do simulador.
    - **Criando e rodando um simples aplicativo Flutter**
        1. Crie um novo aplicativo Flutter rodando o seguinte código no terminal (abra o terminal no diretório que deseja criar o projeto): 

            ```bash
            flutter create my_app
            ```

        2. Um diretório `my_app` será criado, contendo o aplicativo inicial do Flutter. Entre nesse diretório: 

            ```bash
            cd my_app
            ```

        3. Para rodar o aplicativo no simulador, certifique-se de que o simulador está aberto e então rode: 

            ```bash
            flutter run
            ```

    ### Configuração Android

    - **Instale o Android Studio**
        1. Baixe e instale o [Android Studio](https://developer.android.com/studio).
        2. Inicie o Android Studio e vá pelo 'Android Studio Setup Wizard'‎. Isso instala a versão mais recente do Android SDK, Android SDK Command-line Tools e Android SDK Build-Tools, que são necessárias para o Flutter quando desenvolvemos para Android.
    - **Configurando um dispositivo físico Android**

        Para que seja possível usar o seu dispositivo físico, é necessário realizar uma configuração bem simples nele. Basta ir em configurações e procurar pelas **opções de desenvolvedor**. Se você não encontrou, é provável que esteja com um nome diferente porém semelhante ou pode ser também que seja necessário desbloquear essa opção. Na maioria dos dispositivos, essa opção pode ser desbloqueada clicando três vezes seguidas na versão do sistema do seu aparelho (que pode ser encontrada dentro da opção **Sobre**) lembrando que esse caminho pode mudar de acordo com o seu dispositivo.

        Com as **opções de desenvolvedor** desbloqueadas, basta ir até ela, procurar pela opção **Depuração USB** e deixá-la ativa.

        Para garantir que deu tudo certo, conecte seu dispositivo Android no computador, confirme as permissões que forem solicitadas caso apareça (essas são para a depuração USB que você ativou) e rode no terminal o comando `flutter devices`. 

        Se o seu dispositivo foi exibido no terminal, então significa que deu tudo certo.

    - **Configurando um emulador Android**
        1. Abra o **Android Studio**, clique em **Configure** e em **AVD Manager** e então clique em **Create Virtual Device...**;
        2. Escolha um dispositivo como, por exemplo, o Pixel 2 e clique em **Next**.
        3. Na próxima tela você poderá escolher a versão da API para o Android. Recomenda-se escolher a penúltima versão (que no meu caso é a **Q** API 29) e caso ainda não esteja baixada, clique em **Download** e com o download finalizado, clique em **Finish**. Agora deixe a versão baixada selecionada e clique em **Next.**
        4. Verifique se a configuração está correta e clique em **Finish**.
        5. De volta na tela do **AVD Manager**, inicie o emulador clicando no botão de executar. O emulador será iniciado.

    ### Finalizando

    Agora com o sistema configurado, o próximo passo é [configurar o Visual Studio Code]().

# Configurando o Visual Studio Code

Antes de seguir com a configuração do Visual Studio Code, é importante que você tenha configurado o seu ambiente de acordo com o guia para a sua plataforma, disponível [nessa seção]().

Se você ainda não possui o VS Code instalado, pode realizar a instalação através da seguinte página:

[Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)

Com o VS Code instalado, vá no botão de extensões, pesquise por "flutter" e clique em instalar:

![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2016.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2016.png)

Agora é só apertar `Ctrl + Shift + P` (ou `⌘ + Shift + P` se você estiver no Mac) e colar `Flutter: Run Flutter Doctor` na caixa de diálogo e apertar **Enter** para executar o comando: 

![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2017.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Untitled%2017.png)

Isso vai fazer algumas checagens na configuração do seu ambiente Flutter e um relatório será mostrado. Se alguma solicitação for requisitava no próprio VS Code, basta aceitá-la.
Se alguma coisa relacionada ao Chrome der erro, provavelmente é porque você não possui o Google Chrome instalado, mas não se preocupe, isso não vai te atrapalhar e caso necessário, basta instalar o browser e rodar o mesmo comando (ou você pode rodar `flutter doctor` diretamente no terminal para exibir o mesmo relatório).

## Testando tudo

Com o VS Code configurado, podemos partir para a criação de um projeto com o template básico do Flutter apenas para assegurar que está tudo funcionando.
Para isso, no VS Code:

1. Aperte `Ctrl + Shift + P` (ou `⌘ + Shift + P` caso esteja no Mac);
2. Digite `Flutter: New Application Project` e dê enter;
3. Escolha a pasta onde o projeto deve ser criado, e no VS Code escolha um nome qualquer para o projeto;
4. Caso o VS Code apresente algum erro ao criar o projeto, informando que não foi possível encontrar o SDK do Flutter, basta clicar no botão para selecionar a pasta onde você extraiu o SDK (em caso de instalação manual) e repetir os passos novamente;
5. Com a aplicação criada, basta abrir o emulador ou conectar, o seu dispositivo Android no computador com a depuração USB ativada e apertar `F5` no VS Code para que a aplicação seja instalada no dispositivo (repare que em dispositivos físicos, uma permissão será solicitada antes de instalar a aplicação). Essa primeira execução pode demorar um pouco mas isso é normal.
Ao final, você deve se deparar com a seguinte aplicação: 

![Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Screenshot_1618023800.png](Configurac%CC%A7o%CC%83es%20do%20ambiente%20491f5e7b6cef4d92893280f7e7ba8812/Screenshot_1618023800.png)

---