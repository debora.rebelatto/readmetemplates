# :file_folder: Project Name

Here's a project description.
This is a README file that helps developers to get started with the that they are working with.
It is made using md and some HTML.

``` bash
git clone <url>
```

## Requirements
For development, you will only need Node.js and a node global package, npm, installed in your environment.


## Install
``` bash
cd event-manager-back
npm install
```

## Running the project
``` bash
npm start
```
