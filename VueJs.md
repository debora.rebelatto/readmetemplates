# :file_folder: Project name

Here's a project description.
This is a README file that helps developers to get started with the that they are working with.
It is made using md and some HTML.

``` bash
git clone <url>
```

## Build Setup
``` bash
cd folder-name

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
